# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.94.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.93.0...v1.94.0) (2021-03-12)


### Features

* new modules ([5ddc694](https://gitlab.com/DEIP/deip-client-modules/commit/5ddc69459eb66e1703742d39aaa64b0022cfa8b8))





# [1.93.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.92.0...v1.93.0) (2021-03-09)


### Features

* **@deip/research-service:** Research members edit fix ([cc8bc35](https://gitlab.com/DEIP/deip-client-modules/commit/cc8bc353d82cb8362c4632a3410552d05c9d2034))





# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)


### Features

* **@deip/research-nda-service:** Research nda list of projects ([24e9a85](https://gitlab.com/DEIP/deip-client-modules/commit/24e9a855d238afa6b6fe19d2a0bc308963a5b1f6))





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)


### Features

* **@deip/research-nda-service:** Optional field; new method ([2843189](https://gitlab.com/DEIP/deip-client-modules/commit/284318937182de7aca4449a324cc0e3f02ec9d81))





# [1.90.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.89.0...v1.90.0) (2021-02-24)


### Features

* **@deip/research-service:** Research group owner for TTO is set to ([9a15de7](https://gitlab.com/DEIP/deip-client-modules/commit/9a15de755e58c1a0152d67adf05d0b000cccbbce))





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)


### Features

* **@deip/research-nda-service:** Transaction tenant signature ([29ede55](https://gitlab.com/DEIP/deip-client-modules/commit/29ede55b92bf126f176acb42515019b3af802d05))





## [1.88.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.0...v1.88.1) (2021-02-19)

**Note:** Version bump only for package library





# [1.88.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.87.0...v1.88.0) (2021-02-18)


### Features

* **@deip/research-nda-service:** Tenant signature ([413b775](https://gitlab.com/DEIP/deip-client-modules/commit/413b77559aedc3fb4427a2da7770703fe38e7e25))





# [1.87.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.86.0...v1.87.0) (2021-02-17)


### Features

* **@deip/research-nda-service:** NDA signatures ([9a76a45](https://gitlab.com/DEIP/deip-client-modules/commit/9a76a45ed4160707e44c51049e81d67430532799))





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)


### Features

* **@deip/research-nda-service:** NDA contracts ([6ec5d01](https://gitlab.com/DEIP/deip-client-modules/commit/6ec5d01244b9179b37c7e66fb61abbe3bff06518))





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)


### Bug Fixes

* mergeDeep ([e2a5859](https://gitlab.com/DEIP/deip-client-modules/commit/e2a58595b3f82dbae1f74ac3b284150c8eb1b57c))





# [1.85.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.84.0...v1.85.0) (2021-02-15)


### Features

* **@deip/tenant-service:** Tenants network api ([83eb7d3](https://gitlab.com/DEIP/deip-client-modules/commit/83eb7d335bd44cebd0b5afa3f57d0a4100b6d96c))





# [1.84.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.83.0...v1.84.0) (2021-02-12)


### Features

* **@deip/tenant-service:** Tenants network ([2300c95](https://gitlab.com/DEIP/deip-client-modules/commit/2300c95a857f375a06febcd08032adcd5e6ff74f))





# [1.83.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.82.0...v1.83.0) (2021-02-10)


### Features

* **@deip/grants-service:** GTD demo recovered ([1c150a4](https://gitlab.com/DEIP/deip-client-modules/commit/1c150a40f0d5e1ded296b33657dcb7a3f9b301cd))





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)


### Features

* **@deip/express-licensing-service:** Express licensing fixed ([4a81f8d](https://gitlab.com/DEIP/deip-client-modules/commit/4a81f8d80fdb1729e047db719be27440c278c946))





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)


### Features

* **@deip/users-service:** deipRpc calls moved to the server ([3787fd6](https://gitlab.com/DEIP/deip-client-modules/commit/3787fd69117ddec1121000611c4282fad183af13))





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)


### Features

* **@deip/users-service:** moving deipRpc to the server ([c6b1d6e](https://gitlab.com/DEIP/deip-client-modules/commit/c6b1d6ec48f6a94b27abb0724e49ba2d63a57e0c))





## [1.79.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.0...v1.79.1) (2021-01-25)


### Bug Fixes

* **@deip/validation-plugin:** fix package main ([eb226d3](https://gitlab.com/DEIP/deip-client-modules/commit/eb226d3f7140accbb614c5af9e81ccd84055d248))





# [1.79.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.78.0...v1.79.0) (2021-01-22)


### Features

* **@deip/users-service:** getting users by research group ([11f2747](https://gitlab.com/DEIP/deip-client-modules/commit/11f2747abf7e75db7c8f3b3bf34089bc2908344c))





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)


### Features

* **@deip/app-config-service:** convert to @deip/proxydi wrapper ([3b8877a](https://gitlab.com/DEIP/deip-client-modules/commit/3b8877a34b9843e9ad494d78c8f296ada0adb7c3))
* **@deip/auth-module:** release ([fc7ef57](https://gitlab.com/DEIP/deip-client-modules/commit/fc7ef57b3b17a121b198770462b980fe2966eed5))
* **@deip/blockchain-service:** switch to @deip/proxydi ([e347a4e](https://gitlab.com/DEIP/deip-client-modules/commit/e347a4e9bf4fd4fd13fbbef0189fd89d690288c1))
* **@deip/current-user-module:** release ([bce7ad1](https://gitlab.com/DEIP/deip-client-modules/commit/bce7ad112bd0323a98a62549d2ef7f48c6b1ec02))
* **@deip/env-module:** release ([a744e36](https://gitlab.com/DEIP/deip-client-modules/commit/a744e36ae11afc88bf28695f0d4761d18b74f344))
* **@deip/http-service:** switch to @deip/proxydi ([9d97f86](https://gitlab.com/DEIP/deip-client-modules/commit/9d97f86cd73e4de89ee8859dbcb058821954b43b))
* **@deip/proxydi:** new utility ([9edb533](https://gitlab.com/DEIP/deip-client-modules/commit/9edb533a5eccd786a6eef2470b42576eb166e6de))
* **@deip/toolbox:** add new helpers ([215f192](https://gitlab.com/DEIP/deip-client-modules/commit/215f19222e908c91b28c2f4d511be70fbfc35a2c))
* **@deip/users-module:** release ([62cd14f](https://gitlab.com/DEIP/deip-client-modules/commit/62cd14f59984f1460f1c8e23abba0084e2f45963))
* **@deip/users-service:** add alternative method getUserRe (temp) ([afcf093](https://gitlab.com/DEIP/deip-client-modules/commit/afcf093c45e729d5896bb988573123aab91e9250))
* **@deip/validation-plugin:** release ([10641fe](https://gitlab.com/DEIP/deip-client-modules/commit/10641fe3f58a334bc35d92c3f1b8d44ad28b3636))
* **@deip/vuetify-extended:** release ([2cf8bb6](https://gitlab.com/DEIP/deip-client-modules/commit/2cf8bb665e4328f232cd523588ec6db21a666be6))





# [1.77.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.76.0...v1.77.0) (2021-01-14)


### Features

* **@deip/disciplines-service:** claims removed, plugin api added ([b35b45a](https://gitlab.com/DEIP/deip-client-modules/commit/b35b45a279ff023f2b9a3730340a3aa05164fa45))





# [1.76.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.1...v1.76.0) (2021-01-12)


### Features

* **@deip/disciplines-service:** fetching disciplines ([59964c4](https://gitlab.com/DEIP/deip-client-modules/commit/59964c428ed78af4a0d23ecfdd014155b3f37b29))





## [1.75.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.0...v1.75.1) (2021-01-05)

**Note:** Version bump only for package library





# [1.75.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.74.1...v1.75.0) (2020-12-29)


### Bug Fixes

* add return to getEnrichedProfiles ([87579ac](https://gitlab.com/DEIP/deip-client-modules/commit/87579accd04be5dd5ffc383c9a303b7604c51abf))


### Features

* **@deip/users-service:** getting user by email api ([d9c8126](https://gitlab.com/DEIP/deip-client-modules/commit/d9c8126fbcc31643b3691a63c33e1505221f1981))





## [1.74.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.74.0...v1.74.1) (2020-12-18)


### Bug Fixes

* add username to user object ([4faa6b4](https://gitlab.com/DEIP/deip-client-modules/commit/4faa6b40cb62ecedf1ea01cb5663685bc7599089))





# [1.74.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.73.0...v1.74.0) (2020-12-18)


### Features

* add team <-> user info to responce ([4a3f77a](https://gitlab.com/DEIP/deip-client-modules/commit/4a3f77a5edcbbd3306de5f36fcfe8d9bd7440fe6))





# [1.73.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.72.1...v1.73.0) (2020-12-18)


### Features

* getTeamsByUser, getUsersByTeam ([5129d22](https://gitlab.com/DEIP/deip-client-modules/commit/5129d22dbf48f7d3a7581f1554514b4a6bd2cd42))





## [1.72.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.72.0...v1.72.1) (2020-12-17)

**Note:** Version bump only for package library





# [1.72.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.71.0...v1.72.0) (2020-12-17)


### Features

* add getUser method ([80ff914](https://gitlab.com/DEIP/deip-client-modules/commit/80ff914d7804ca331ddca73ec5c7ff8849cb8d51))





# [1.71.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.70.0...v1.71.0) (2020-12-15)


### Features

* **@deip/users-service:** debugger removed ([1502162](https://gitlab.com/DEIP/deip-client-modules/commit/1502162f9ddde517b5d73c743037c24a27e58f69))





# [1.70.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.69.0...v1.70.0) (2020-12-15)


### Features

* **@deip/users-service:** Active users getter fixed ([e905d98](https://gitlab.com/DEIP/deip-client-modules/commit/e905d98b84e4c31c31af9db354725838975373c8))





# [1.69.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.68.0...v1.69.0) (2020-12-10)


### Features

* **@deip/research-service:** References graph ([b702f4a](https://gitlab.com/DEIP/deip-client-modules/commit/b702f4a3145bd49b10b9d39bf4fb401376b5a24d))





# [1.68.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.67.0...v1.68.0) (2020-12-09)


### Features

* **@deip/research-content-reviews-service:** Review criterias ([97b0639](https://gitlab.com/DEIP/deip-client-modules/commit/97b0639ca1007ba5dc447400014cde9bfdf6a45c))





# [1.67.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.66.0...v1.67.0) (2020-12-08)


### Features

* **@deip/assets-service:** Security token asset ([cef6d02](https://gitlab.com/DEIP/deip-client-modules/commit/cef6d025ae9d2ba3f72574ccbb0a8853d26f66d0))





# [1.66.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.65.0...v1.66.0) (2020-12-08)


### Features

* **@deip/research-service:** Licensing revenue share ([007b8ec](https://gitlab.com/DEIP/deip-client-modules/commit/007b8ec7e9cfad6cf8cff4064c11d4f7f513b89d))





# [1.65.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.64.0...v1.65.0) (2020-12-07)


### Features

* **@deip/research-service:** Research, ResearchContent, Review, Research Group meta ([6b70900](https://gitlab.com/DEIP/deip-client-modules/commit/6b70900e8ea2cc7c191d53434a6fca21b5d3a1ae))





# [1.64.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.63.0...v1.64.0) (2020-12-03)


### Features

* **@deip/research-service:** Research service hot fix 2 ([d531344](https://gitlab.com/DEIP/deip-client-modules/commit/d5313447b8949be9e665d5156fa088d10fa924e9))





# [1.63.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.62.0...v1.63.0) (2020-12-03)


### Features

* **@deip/research-service:** Research service hot fix ([daa7116](https://gitlab.com/DEIP/deip-client-modules/commit/daa711695ccb797daa8a8d545565d90a1411f992))





# [1.62.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.61.0...v1.62.0) (2020-12-03)


### Features

* **@deip/research-group-service:** Research group metadata ([9cb3dbb](https://gitlab.com/DEIP/deip-client-modules/commit/9cb3dbb8e3c1843e366f3a48cdad1eecba93ce57))





# [1.61.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.60.0...v1.61.0) (2020-11-30)


### Features

* **@deip/grants-service:** Withdraw request auto-approval ([39a2361](https://gitlab.com/DEIP/deip-client-modules/commit/39a236137a3a62354832bc12f2cd12ca02dc6ea9))





# [1.60.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.59.0...v1.60.0) (2020-11-24)


### Features

* **@deip/proposals-service:** Proposals update endpoint ([21ba24b](https://gitlab.com/DEIP/deip-client-modules/commit/21ba24bcc8c67f6e1ecc7b170239d52a55dabca1))





# [1.59.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.58.0...v1.59.0) (2020-11-23)


### Features

* **@deip/proposals-service:** Unified proposals ([99036d3](https://gitlab.com/DEIP/deip-client-modules/commit/99036d39d787f9c839e8aeba33ba6d1dc1545e6a))





# [1.58.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.1...v1.58.0) (2020-11-20)


### Features

* **@deip/assets-service:** Assets exchange and transfer transactions ([2f9236a](https://gitlab.com/DEIP/deip-client-modules/commit/2f9236a23f3735a6e15a6d73599575e1220c1fed))





## [1.57.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.0...v1.57.1) (2020-11-19)

**Note:** Version bump only for package library





# [1.57.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.56.0...v1.57.0) (2020-11-18)


### Features

* **@deip/proposals-service:** Proposals for account ([a03e847](https://gitlab.com/DEIP/deip-client-modules/commit/a03e847bb8a1b8b93dec3d4a77f43fb8a5db9410))





# [1.56.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.55.0...v1.56.0) (2020-11-11)


### Features

* **@deip/research-content-reviews-service:** Temp name for demo ([55d9cb7](https://gitlab.com/DEIP/deip-client-modules/commit/55d9cb725d8ee653ff0f19ca49b191bbfada9da2))





# [1.55.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.54.0...v1.55.0) (2020-11-06)


### Features

* **@deip/assets-service:** Assets revenue api ([f682066](https://gitlab.com/DEIP/deip-client-modules/commit/f682066528930170e9654c2a99a1c3bbeef44c74))





# [1.54.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.53.0...v1.54.0) (2020-11-05)


### Features

* **@deip/assets-service:** Security tokens incorporated to assets ([6ff01ee](https://gitlab.com/DEIP/deip-client-modules/commit/6ff01ee0489a95d321208c7e1196600e38517060))





# [1.53.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.52.0...v1.53.0) (2020-11-04)


### Features

* **@deip/investments-service:** Transfer security token memo ([2305fca](https://gitlab.com/DEIP/deip-client-modules/commit/2305fcac34e8afac55dce606f1e24735eee68f7f))





# [1.52.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.51.0...v1.52.0) (2020-11-02)


### Features

* **@deip/investments-service:** Fundraising fix ([6a69520](https://gitlab.com/DEIP/deip-client-modules/commit/6a695204464aaeda21112c0bf3bd58773042293d))





# [1.51.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.50.0...v1.51.0) (2020-10-27)


### Features

* **@deip/investments-service:** Security tokens ([5931015](https://gitlab.com/DEIP/deip-client-modules/commit/5931015f61d871b022811cc20e032b34a4b451db))





# [1.50.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.49.0...v1.50.0) (2020-10-12)


### Features

* **@deip/user-service:** User transactions ([8b019de](https://gitlab.com/DEIP/deip-client-modules/commit/8b019deafbfea046495c8097e1cb6f2c50840aec))





# [1.49.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.48.0...v1.49.0) (2020-10-09)


### Features

* **@deip/express-licensing-service:** Express licensing feature ([bbbadfc](https://gitlab.com/DEIP/deip-client-modules/commit/bbbadfcdc5773a2214391f761057d5ce013b902a))





# [1.48.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.47.0...v1.48.0) (2020-10-08)


### Features

* **@deip/research-service:** Token sale endpoints ([5d8fb31](https://gitlab.com/DEIP/deip-client-modules/commit/5d8fb31dd39726179ab5dcd3ffa46b32d9a5fcb3))





# [1.47.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.46.0...v1.47.0) (2020-10-06)


### Features

* **@deip/research-service:** Research multipart ([a32cd75](https://gitlab.com/DEIP/deip-client-modules/commit/a32cd750b74043ab5cb005872924e21ec2769c07))





# [1.46.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.45.0...v1.46.0) (2020-10-04)


### Features

* **@deip/research-service:** Invites\Resignations fixed ([edb32ed](https://gitlab.com/DEIP/deip-client-modules/commit/edb32ed650d1532cc1b6890c5b41d16e865da143))





# [1.45.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.44.0...v1.45.0) (2020-10-02)


### Features

* **@deip/research-service:** Research update fixed ([18838f3](https://gitlab.com/DEIP/deip-client-modules/commit/18838f3512454ba67c937a82ab7cd63efb92f646))





# [1.44.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.43.0...v1.44.0) (2020-10-02)


### Features

* **@deip/research-service:** Research edit fixed; Auto-apptovals ([06d26bb](https://gitlab.com/DEIP/deip-client-modules/commit/06d26bba33c603f10423de981f8560e162578c91))





# [1.43.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.42.0...v1.43.0) (2020-09-28)


### Features

* **@deip/research-service:** Invites in research service fixed ([acb6d68](https://gitlab.com/DEIP/deip-client-modules/commit/acb6d68734de82e3314ff6ab680d222d97ec61cb))





# [1.42.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.41.0...v1.42.0) (2020-09-27)


### Features

* **@deip/research-service:** Research group along with research ([4910089](https://gitlab.com/DEIP/deip-client-modules/commit/4910089b78660403de1deca2699d7ae597027f0f))





# [1.41.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.40.0...v1.41.0) (2020-09-21)


### Features

* **@deip/research-service:** Default params for operation ([47bbc70](https://gitlab.com/DEIP/deip-client-modules/commit/47bbc70ec9e9d38e8798db80e9ead6561f66fbfa))





# [1.40.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.39.0...v1.40.0) (2020-09-21)


### Features

* **@deip/research-service:** Camel case for params ([cbe28f5](https://gitlab.com/DEIP/deip-client-modules/commit/cbe28f596b652b5190521ac81422665ea4295e1c))





# [1.39.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.38.0...v1.39.0) (2020-09-18)


### Features

* **@deip/research-service:** Update research meta ([90e17e2](https://gitlab.com/DEIP/deip-client-modules/commit/90e17e2f6cb82a7100dc8528215473b1467f1ad1))





# [1.38.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.37.0...v1.38.0) (2020-09-10)


### Features

* **@deip/research-service:** Server side feed filter ([d402f55](https://gitlab.com/DEIP/deip-client-modules/commit/d402f55bab7297716b275aaa8154f68afaa775b2))





# [1.37.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.36.0...v1.37.0) (2020-08-25)


### Features

* **@deip/research-service:** 'create_review' fixed ([dfa997e](https://gitlab.com/DEIP/deip-client-modules/commit/dfa997e70a49741cfaebdc4b0741dbeb34e11ec2))





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)


### Features

* **@deip/tenant-service:** Research attributes ([b29f7f5](https://gitlab.com/DEIP/deip-client-modules/commit/b29f7f56fb9086827803d7768ffacbd6ab05e0e4))





# [1.35.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.34.0...v1.35.0) (2020-07-18)


### Features

* **@deip/expertise-contributions-service:** Research\ResearchContent ECI stats ([1ccfc59](https://gitlab.com/DEIP/deip-client-modules/commit/1ccfc59dab31021959a305b6dd3d2eb5a17804d9))





# [1.34.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.33.0...v1.34.0) (2020-07-15)


### Features

* **@deip/expertise-contributions-service:** Growth rate starting point ([0d013b5](https://gitlab.com/DEIP/deip-client-modules/commit/0d013b53a0d3a28a813c8e725577b07824f534c5))





# [1.33.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.32.0...v1.33.0) (2020-07-10)


### Features

* **@deip/expertise-contributions-service:** Disciplines ECI history endpoint ([acd85ef](https://gitlab.com/DEIP/deip-client-modules/commit/acd85ef9c6fd5134f2f7cf013225cfbf59ad2acb))





# [1.32.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.31.0...v1.32.0) (2020-07-10)


### Features

* **@deip/expertise-contributions-service:** Disciplines ECI growth rate ([a9eb1a0](https://gitlab.com/DEIP/deip-client-modules/commit/a9eb1a005106aab37db17007752d1880a130d65d))





# [1.31.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.30.0...v1.31.0) (2020-07-09)


### Features

* **@deip/expertise-contributions-service:** Accounts ECI filter ([f11f3c4](https://gitlab.com/DEIP/deip-client-modules/commit/f11f3c4eb1eefc0b076c106f5228b3a9c88b473c))





# [1.30.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.29.0...v1.30.0) (2020-07-02)


### Features

* **@deip/expertise-contributions-service:** Research contents eci stat ([ff0c26e](https://gitlab.com/DEIP/deip-client-modules/commit/ff0c26e040ef05e639533c490bdf29532fe0d055))





# [1.29.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.28.0...v1.29.0) (2020-07-02)


### Features

* **@deip/expertise-contributions-service:** Account eci stat ([652fea8](https://gitlab.com/DEIP/deip-client-modules/commit/652fea8945c26a866d694cbd8559b95ae058f471))





# [1.28.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.27.0...v1.28.0) (2020-07-01)


### Features

* **@deip/rpc-client:** Discipline ECI stats endpoint ([8f6d879](https://gitlab.com/DEIP/deip-client-modules/commit/8f6d87903fce8bf6cfbb53b53834c7d34ab5ab8e))





# [1.27.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.26.0...v1.27.0) (2020-06-30)


### Features

* **@deip/rpc-client:** Accounts ECI stats endpoint filter ([5462606](https://gitlab.com/DEIP/deip-client-modules/commit/546260625203331333382044cd5288d51205e47b))





# [1.26.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.25.0...v1.26.0) (2020-06-28)


### Features

* **@deip/rpc-client:** Accounts ECI stats endpoint ([142ed5c](https://gitlab.com/DEIP/deip-client-modules/commit/142ed5cf0643d1535fede966c9f64cdf55b1ed7b))





# [1.25.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.24.0...v1.25.0) (2020-06-24)


### Features

* **@deip/rpc-client:** fetch all accounts method is deprecated ([7f5b8ee](https://gitlab.com/DEIP/deip-client-modules/commit/7f5b8ee8615193d5257a245e9e7dd5f99dd97a7b))





# [1.24.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.23.0...v1.24.0) (2020-06-22)


### Features

* **@deip/research-service:** Review share percent is optional ([25ac91b](https://gitlab.com/DEIP/deip-client-modules/commit/25ac91b69379342b2164826fc2aff9b1b9fb7f56))





# [1.23.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.22.0...v1.23.0) (2020-06-10)


### Features

* **@deip/grants-service:** Grant disciplines ([bc7475c](https://gitlab.com/DEIP/deip-client-modules/commit/bc7475c3216830c3a823439f3bca2edde8cee3a7))





# [1.22.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.21.0...v1.22.0) (2020-06-09)


### Features

* **@deip/research-service:** Discipline external ids ([04e64aa](https://gitlab.com/DEIP/deip-client-modules/commit/04e64aa25998ed992e12167318d747ae33ddfc96))





# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)


### Features

* **@deip/research-group-service:** Posting authorities replaced with active overrides ([c832136](https://gitlab.com/DEIP/deip-client-modules/commit/c832136a7b0875171db05f69d445d1dd95d11379))





# [1.20.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.19.0...v1.20.0) (2020-05-27)


### Features

* **@deip/research-service:** Research application delete ([722e0cc](https://gitlab.com/DEIP/deip-client-modules/commit/722e0ccb48e7ddf1a57c8ee8ee23a4d660760df2))





# [1.19.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.18.0...v1.19.0) (2020-05-26)


### Features

* **@deip/research-service:** Research criterias ([f52c25e](https://gitlab.com/DEIP/deip-client-modules/commit/f52c25e4552d0f31f5ba42227d46edece4e7a60a))





# [1.18.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.17.0...v1.18.0) (2020-05-24)


### Features

* **@deip/research-service:** Research listing endpoints ([bc07d52](https://gitlab.com/DEIP/deip-client-modules/commit/bc07d52960a3b88aab590dc4d5b74b956d800fb4))





# [1.17.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.16.0...v1.17.0) (2020-05-22)


### Features

* **@deip/research-service:** Permlinks deprecated ([70f9b37](https://gitlab.com/DEIP/deip-client-modules/commit/70f9b37013a284e9ec1d2806952da6713b354cd4))





# [1.16.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.15.0...v1.16.0) (2020-05-21)


### Features

* **@deip/research-service:** Research application edit endpoint ([4467dea](https://gitlab.com/DEIP/deip-client-modules/commit/4467dea45dee80759eed4cc3baf35540cdbc3cc4))





# [1.15.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.14.0...v1.15.0) (2020-05-21)


### Features

* **@deip/research-service:** Research application approve/reject endpoints ([c9ed6ea](https://gitlab.com/DEIP/deip-client-modules/commit/c9ed6ea3d120f45015760cb4b398560e5e0a5a8d))





# [1.14.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.13.0...v1.14.0) (2020-05-20)


### Features

* **@deip/research-service:** Research application endpoints ([ba2985f](https://gitlab.com/DEIP/deip-client-modules/commit/ba2985fae23fd5c7a48d55acb8d7bf5a0e9181d1))





# [1.13.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.12.0...v1.13.0) (2020-05-18)


### Features

* **@deip/rpc-client:** Research share transfer operation ([020f824](https://gitlab.com/DEIP/deip-client-modules/commit/020f8243189f425915ac70cf8cb096a466798b74))





# [1.12.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.11.0...v1.12.0) (2020-05-15)


### Features

* **@deip/tenant-service:** Tenant sign-in; ([9c17a98](https://gitlab.com/DEIP/deip-client-modules/commit/9c17a98222fbe61e1bab86814e1714645674ce44))





# [1.11.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.10.0...v1.11.0) (2020-05-14)


### Features

* **@deip/research-service:** Tenant dynamic criterias; ([f5fbd6e](https://gitlab.com/DEIP/deip-client-modules/commit/f5fbd6e7a4e59df5700d6125595d7ac06fe0413a))





# [1.10.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.9.0...v1.10.0) (2020-05-14)


### Features

* **@deip/proposals-service:** External id serializer; Services api params ([2c86e03](https://gitlab.com/DEIP/deip-client-modules/commit/2c86e03882b048bc0f20a754008188007bf5d4ef))





# [1.9.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.8.0...v1.9.0) (2020-05-07)


### Features

* **@deip/tenant-service:** Tenant updates ([4118f95](https://gitlab.com/DEIP/deip-client-modules/commit/4118f9574d5dbc791c0bad503704ae6ee90c3ed0))





# [1.8.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.7.0...v1.8.0) (2020-05-06)


### Features

* **@deip/tenant-service:** Tenant signups ([189d8b1](https://gitlab.com/DEIP/deip-client-modules/commit/189d8b1fe70c790194c739ef2014fa37f907d6d7))





# [1.7.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.6.0...v1.7.0) (2020-05-04)


### Features

* **@deip/proposals-service:** Multisignature proposals ([a63a9b9](https://gitlab.com/DEIP/deip-client-modules/commit/a63a9b90f275718ef0b2ef13390c1d9d1391d84c))





# [1.6.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.5.0...v1.6.0) (2020-04-13)


### Features

* **@deip/grants-service:** Grant award withdrawal request api ([ea3e17c](https://gitlab.com/DEIP/deip-client-modules/commit/ea3e17c007b7df29ffdc001d73cd725c705c25ee))





# [1.5.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.4.0...v1.5.0) (2020-04-03)


### Features

* **@deip/grants-service:** Grants transparency operations ([bd882c7](https://gitlab.com/DEIP/deip-client-modules/commit/bd882c7c3bcc823f3f46246def15efc3af9620cb))





# [1.4.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.3.0...v1.4.0) (2020-03-31)


### Features

* **@deip/grants-service:** Awardee and awards api ([814ac0a](https://gitlab.com/DEIP/deip-client-modules/commit/814ac0adc0615f6ef84332d1d6c7c350d0595542))





# [1.3.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.2.0...v1.3.0) (2020-03-30)


### Features

* **@deip/expertise-contributions-service:** Expertise contributions service ([4d87bd0](https://gitlab.com/DEIP/deip-client-modules/commit/4d87bd039319569be494bd9249548451d3596aff))





# [1.2.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.1.0...v1.2.0) (2020-03-23)


### Features

* **@deip/grants-service:** funding opportunity announcements ([be9a4dd](https://gitlab.com/DEIP/deip-client-modules/commit/be9a4dd54f738088f8bf9109532b3033923d15a6))





# 1.1.0 (2020-03-20)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))


### Features

* **@deip/research-content-reviews-service:** generic assessment model for review ([7e543e2](https://gitlab.com/DEIP/deip-client-modules/commit/7e543e224f93ada160f2c91a133db0cba5c0f5b2))


### Reverts

* Revert "Fix create contract params names" ([dc01779](https://gitlab.com/DEIP/deip-client-modules/commit/dc017794c4209d40f5528f76048e37811f62f6ac))





# 1.0.0 (2020-03-19)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))


### Reverts

* Revert "Fix create contract params names" ([dc01779](https://gitlab.com/DEIP/deip-client-modules/commit/dc017794c4209d40f5528f76048e37811f62f6ac))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))


### Reverts

* Revert "Fix create contract params names" ([e4a7352](https://gitlab.com/DEIP/deip-client-modules/commit/e4a7352368e4e01411c9d4d1e62834ff4dd53b17))
