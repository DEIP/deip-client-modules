# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/proxydi





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)


### Features

* **@deip/proxydi:** new utility ([9edb533](https://gitlab.com/DEIP/deip-client-modules/commit/9edb533a5eccd786a6eef2470b42576eb166e6de))





# 1.1.0 (2020-03-20)

**Note:** Version bump only for package @deip/app-config-service





# 1.0.0 (2020-03-19)

**Note:** Version bump only for package @deip/app-config-service





# 1.0.0 (2020-03-18)

**Note:** Version bump only for package @deip/app-config-service
