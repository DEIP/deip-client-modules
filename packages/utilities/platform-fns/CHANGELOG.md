# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.94.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.93.0...v1.94.0) (2021-03-12)


### Features

* new modules ([5ddc694](https://gitlab.com/DEIP/deip-client-modules/commit/5ddc69459eb66e1703742d39aaa64b0022cfa8b8))
