# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)


### Features

* **@deip/research-group-service:** Posting authorities replaced with active overrides ([c832136](https://gitlab.com/DEIP/deip-client-modules/commit/c832136a7b0875171db05f69d445d1dd95d11379))





# 1.1.0 (2020-03-20)

**Note:** Version bump only for package @deip/lib-crypto





# 1.0.0 (2020-03-19)

**Note:** Version bump only for package @deip/lib-crypto





# 1.0.0 (2020-03-18)

**Note:** Version bump only for package @deip/lib-crypto
