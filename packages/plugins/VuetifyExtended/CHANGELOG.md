# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.94.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.93.0...v1.94.0) (2021-03-12)


### Features

* new modules ([5ddc694](https://gitlab.com/DEIP/deip-client-modules/commit/5ddc69459eb66e1703742d39aaa64b0022cfa8b8))





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/vuetify-extended





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)


### Features

* **@deip/vuetify-extended:** release ([2cf8bb6](https://gitlab.com/DEIP/deip-client-modules/commit/2cf8bb665e4328f232cd523588ec6db21a666be6))
