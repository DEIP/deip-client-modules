# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/validation-plugin





## [1.79.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.0...v1.79.1) (2021-01-25)


### Bug Fixes

* **@deip/validation-plugin:** fix package main ([eb226d3](https://gitlab.com/DEIP/deip-client-modules/commit/eb226d3f7140accbb614c5af9e81ccd84055d248))





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)


### Features

* **@deip/validation-plugin:** release ([10641fe](https://gitlab.com/DEIP/deip-client-modules/commit/10641fe3f58a334bc35d92c3f1b8d44ad28b3636))
