export * from './lib/AuthModule';

export { default as AuthSignIn } from './lib/components/AuthSignIn';
export { default as AuthSignUp } from './lib/components/AuthSignUp';
export { default as AuthSignOut } from './lib/components/AuthSignOut';
