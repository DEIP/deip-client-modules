# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.94.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.93.0...v1.94.0) (2021-03-12)


### Features

* new modules ([5ddc694](https://gitlab.com/DEIP/deip-client-modules/commit/5ddc69459eb66e1703742d39aaa64b0022cfa8b8))





# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)

**Note:** Version bump only for package @deip/auth-module





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)

**Note:** Version bump only for package @deip/auth-module





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)

**Note:** Version bump only for package @deip/auth-module





## [1.88.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.0...v1.88.1) (2021-02-19)

**Note:** Version bump only for package @deip/auth-module





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)

**Note:** Version bump only for package @deip/auth-module





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/auth-module





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)

**Note:** Version bump only for package @deip/auth-module





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)

**Note:** Version bump only for package @deip/auth-module





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)

**Note:** Version bump only for package @deip/auth-module





## [1.79.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.0...v1.79.1) (2021-01-25)


### Bug Fixes

* **@deip/validation-plugin:** fix package main ([eb226d3](https://gitlab.com/DEIP/deip-client-modules/commit/eb226d3f7140accbb614c5af9e81ccd84055d248))





# [1.79.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.78.0...v1.79.0) (2021-01-22)

**Note:** Version bump only for package @deip/auth-module





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)


### Features

* **@deip/auth-module:** release ([fc7ef57](https://gitlab.com/DEIP/deip-client-modules/commit/fc7ef57b3b17a121b198770462b980fe2966eed5))
