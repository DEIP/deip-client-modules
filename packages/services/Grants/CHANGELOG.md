# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/grants-service





# [1.83.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.82.0...v1.83.0) (2021-02-10)


### Features

* **@deip/grants-service:** GTD demo recovered ([1c150a4](https://gitlab.com/DEIP/deip-client-modules/commit/1c150a40f0d5e1ded296b33657dcb7a3f9b301cd))





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/grants-service





# [1.61.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.60.0...v1.61.0) (2020-11-30)


### Features

* **@deip/grants-service:** Withdraw request auto-approval ([39a2361](https://gitlab.com/DEIP/deip-client-modules/commit/39a236137a3a62354832bc12f2cd12ca02dc6ea9))





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)

**Note:** Version bump only for package @deip/grants-service





# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)


### Features

* **@deip/research-group-service:** Posting authorities replaced with active overrides ([c832136](https://gitlab.com/DEIP/deip-client-modules/commit/c832136a7b0875171db05f69d445d1dd95d11379))





# [1.6.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.5.0...v1.6.0) (2020-04-13)


### Features

* **@deip/grants-service:** Grant award withdrawal request api ([ea3e17c](https://gitlab.com/DEIP/deip-client-modules/commit/ea3e17c007b7df29ffdc001d73cd725c705c25ee))





# [1.5.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.4.0...v1.5.0) (2020-04-03)


### Features

* **@deip/grants-service:** Grants transparency operations ([bd882c7](https://gitlab.com/DEIP/deip-client-modules/commit/bd882c7c3bcc823f3f46246def15efc3af9620cb))





# [1.4.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.3.0...v1.4.0) (2020-03-31)


### Features

* **@deip/grants-service:** Awardee and awards api ([814ac0a](https://gitlab.com/DEIP/deip-client-modules/commit/814ac0adc0615f6ef84332d1d6c7c350d0595542))





# [1.2.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.1.0...v1.2.0) (2020-03-23)


### Features

* **@deip/grants-service:** funding opportunity announcements ([be9a4dd](https://gitlab.com/DEIP/deip-client-modules/commit/be9a4dd54f738088f8bf9109532b3033923d15a6))





# 1.1.0 (2020-03-20)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-19)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))
