# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.94.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.93.0...v1.94.0) (2021-03-12)

**Note:** Version bump only for package @deip/research-service





# [1.93.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.92.0...v1.93.0) (2021-03-09)


### Features

* **@deip/research-service:** Research members edit fix ([cc8bc35](https://gitlab.com/DEIP/deip-client-modules/commit/cc8bc353d82cb8362c4632a3410552d05c9d2034))





# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)

**Note:** Version bump only for package @deip/research-service





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)

**Note:** Version bump only for package @deip/research-service





# [1.90.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.89.0...v1.90.0) (2021-02-24)


### Features

* **@deip/research-service:** Research group owner for TTO is set to ([9a15de7](https://gitlab.com/DEIP/deip-client-modules/commit/9a15de755e58c1a0152d67adf05d0b000cccbbce))





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)


### Features

* **@deip/research-nda-service:** Transaction tenant signature ([29ede55](https://gitlab.com/DEIP/deip-client-modules/commit/29ede55b92bf126f176acb42515019b3af802d05))





## [1.88.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.0...v1.88.1) (2021-02-19)

**Note:** Version bump only for package @deip/research-service





# [1.88.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.87.0...v1.88.0) (2021-02-18)

**Note:** Version bump only for package @deip/research-service





# [1.87.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.86.0...v1.87.0) (2021-02-17)

**Note:** Version bump only for package @deip/research-service





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)

**Note:** Version bump only for package @deip/research-service





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/research-service





# [1.84.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.83.0...v1.84.0) (2021-02-12)


### Features

* **@deip/tenant-service:** Tenants network ([2300c95](https://gitlab.com/DEIP/deip-client-modules/commit/2300c95a857f375a06febcd08032adcd5e6ff74f))





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)


### Features

* **@deip/express-licensing-service:** Express licensing fixed ([4a81f8d](https://gitlab.com/DEIP/deip-client-modules/commit/4a81f8d80fdb1729e047db719be27440c278c946))





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)


### Features

* **@deip/users-service:** deipRpc calls moved to the server ([3787fd6](https://gitlab.com/DEIP/deip-client-modules/commit/3787fd69117ddec1121000611c4282fad183af13))





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)


### Features

* **@deip/users-service:** moving deipRpc to the server ([c6b1d6e](https://gitlab.com/DEIP/deip-client-modules/commit/c6b1d6ec48f6a94b27abb0724e49ba2d63a57e0c))





# [1.79.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.78.0...v1.79.0) (2021-01-22)

**Note:** Version bump only for package @deip/research-service





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/research-service





# [1.77.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.76.0...v1.77.0) (2021-01-14)


### Features

* **@deip/disciplines-service:** claims removed, plugin api added ([b35b45a](https://gitlab.com/DEIP/deip-client-modules/commit/b35b45a279ff023f2b9a3730340a3aa05164fa45))





## [1.75.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.0...v1.75.1) (2021-01-05)

**Note:** Version bump only for package @deip/research-service





# [1.75.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.74.1...v1.75.0) (2020-12-29)

**Note:** Version bump only for package @deip/research-service





## [1.74.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.74.0...v1.74.1) (2020-12-18)

**Note:** Version bump only for package @deip/research-service





# [1.74.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.73.0...v1.74.0) (2020-12-18)

**Note:** Version bump only for package @deip/research-service





# [1.73.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.72.1...v1.73.0) (2020-12-18)

**Note:** Version bump only for package @deip/research-service





## [1.72.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.72.0...v1.72.1) (2020-12-17)

**Note:** Version bump only for package @deip/research-service





# [1.72.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.71.0...v1.72.0) (2020-12-17)

**Note:** Version bump only for package @deip/research-service





# [1.71.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.70.0...v1.71.0) (2020-12-15)

**Note:** Version bump only for package @deip/research-service





# [1.70.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.69.0...v1.70.0) (2020-12-15)

**Note:** Version bump only for package @deip/research-service





# [1.69.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.68.0...v1.69.0) (2020-12-10)


### Features

* **@deip/research-service:** References graph ([b702f4a](https://gitlab.com/DEIP/deip-client-modules/commit/b702f4a3145bd49b10b9d39bf4fb401376b5a24d))





# [1.67.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.66.0...v1.67.0) (2020-12-08)


### Features

* **@deip/assets-service:** Security token asset ([cef6d02](https://gitlab.com/DEIP/deip-client-modules/commit/cef6d025ae9d2ba3f72574ccbb0a8853d26f66d0))





# [1.66.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.65.0...v1.66.0) (2020-12-08)


### Features

* **@deip/research-service:** Licensing revenue share ([007b8ec](https://gitlab.com/DEIP/deip-client-modules/commit/007b8ec7e9cfad6cf8cff4064c11d4f7f513b89d))





# [1.65.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.64.0...v1.65.0) (2020-12-07)


### Features

* **@deip/research-service:** Research, ResearchContent, Review, Research Group meta ([6b70900](https://gitlab.com/DEIP/deip-client-modules/commit/6b70900e8ea2cc7c191d53434a6fca21b5d3a1ae))





# [1.64.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.63.0...v1.64.0) (2020-12-03)


### Features

* **@deip/research-service:** Research service hot fix 2 ([d531344](https://gitlab.com/DEIP/deip-client-modules/commit/d5313447b8949be9e665d5156fa088d10fa924e9))





# [1.63.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.62.0...v1.63.0) (2020-12-03)


### Features

* **@deip/research-service:** Research service hot fix ([daa7116](https://gitlab.com/DEIP/deip-client-modules/commit/daa711695ccb797daa8a8d545565d90a1411f992))





# [1.62.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.61.0...v1.62.0) (2020-12-03)


### Features

* **@deip/research-group-service:** Research group metadata ([9cb3dbb](https://gitlab.com/DEIP/deip-client-modules/commit/9cb3dbb8e3c1843e366f3a48cdad1eecba93ce57))





# [1.59.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.58.0...v1.59.0) (2020-11-23)


### Features

* **@deip/proposals-service:** Unified proposals ([99036d3](https://gitlab.com/DEIP/deip-client-modules/commit/99036d39d787f9c839e8aeba33ba6d1dc1545e6a))





# [1.58.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.1...v1.58.0) (2020-11-20)

**Note:** Version bump only for package @deip/research-service





## [1.57.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.0...v1.57.1) (2020-11-19)

**Note:** Version bump only for package @deip/research-service





# [1.57.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.56.0...v1.57.0) (2020-11-18)

**Note:** Version bump only for package @deip/research-service





# [1.55.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.54.0...v1.55.0) (2020-11-06)

**Note:** Version bump only for package @deip/research-service





# [1.54.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.53.0...v1.54.0) (2020-11-05)


### Features

* **@deip/assets-service:** Security tokens incorporated to assets ([6ff01ee](https://gitlab.com/DEIP/deip-client-modules/commit/6ff01ee0489a95d321208c7e1196600e38517060))





# [1.53.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.52.0...v1.53.0) (2020-11-04)


### Features

* **@deip/investments-service:** Transfer security token memo ([2305fca](https://gitlab.com/DEIP/deip-client-modules/commit/2305fcac34e8afac55dce606f1e24735eee68f7f))





# [1.52.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.51.0...v1.52.0) (2020-11-02)


### Features

* **@deip/investments-service:** Fundraising fix ([6a69520](https://gitlab.com/DEIP/deip-client-modules/commit/6a695204464aaeda21112c0bf3bd58773042293d))





# [1.51.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.50.0...v1.51.0) (2020-10-27)


### Features

* **@deip/investments-service:** Security tokens ([5931015](https://gitlab.com/DEIP/deip-client-modules/commit/5931015f61d871b022811cc20e032b34a4b451db))





# [1.48.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.47.0...v1.48.0) (2020-10-08)

**Note:** Version bump only for package @deip/research-service





# [1.47.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.46.0...v1.47.0) (2020-10-06)


### Features

* **@deip/research-service:** Research multipart ([a32cd75](https://gitlab.com/DEIP/deip-client-modules/commit/a32cd750b74043ab5cb005872924e21ec2769c07))





# [1.46.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.45.0...v1.46.0) (2020-10-04)


### Features

* **@deip/research-service:** Invites\Resignations fixed ([edb32ed](https://gitlab.com/DEIP/deip-client-modules/commit/edb32ed650d1532cc1b6890c5b41d16e865da143))





# [1.45.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.44.0...v1.45.0) (2020-10-02)


### Features

* **@deip/research-service:** Research update fixed ([18838f3](https://gitlab.com/DEIP/deip-client-modules/commit/18838f3512454ba67c937a82ab7cd63efb92f646))





# [1.44.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.43.0...v1.44.0) (2020-10-02)


### Features

* **@deip/research-service:** Research edit fixed; Auto-apptovals ([06d26bb](https://gitlab.com/DEIP/deip-client-modules/commit/06d26bba33c603f10423de981f8560e162578c91))





# [1.43.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.42.0...v1.43.0) (2020-09-28)


### Features

* **@deip/research-service:** Invites in research service fixed ([acb6d68](https://gitlab.com/DEIP/deip-client-modules/commit/acb6d68734de82e3314ff6ab680d222d97ec61cb))





# [1.42.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.41.0...v1.42.0) (2020-09-27)


### Features

* **@deip/research-service:** Research group along with research ([4910089](https://gitlab.com/DEIP/deip-client-modules/commit/4910089b78660403de1deca2699d7ae597027f0f))





# [1.41.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.40.0...v1.41.0) (2020-09-21)


### Features

* **@deip/research-service:** Default params for operation ([47bbc70](https://gitlab.com/DEIP/deip-client-modules/commit/47bbc70ec9e9d38e8798db80e9ead6561f66fbfa))





# [1.40.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.39.0...v1.40.0) (2020-09-21)


### Features

* **@deip/research-service:** Camel case for params ([cbe28f5](https://gitlab.com/DEIP/deip-client-modules/commit/cbe28f596b652b5190521ac81422665ea4295e1c))





# [1.39.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.38.0...v1.39.0) (2020-09-18)


### Features

* **@deip/research-service:** Update research meta ([90e17e2](https://gitlab.com/DEIP/deip-client-modules/commit/90e17e2f6cb82a7100dc8528215473b1467f1ad1))





# [1.38.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.37.0...v1.38.0) (2020-09-10)


### Features

* **@deip/research-service:** Server side feed filter ([d402f55](https://gitlab.com/DEIP/deip-client-modules/commit/d402f55bab7297716b275aaa8154f68afaa775b2))





# [1.37.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.36.0...v1.37.0) (2020-08-25)

**Note:** Version bump only for package @deip/research-service





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)

**Note:** Version bump only for package @deip/research-service





# [1.35.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.34.0...v1.35.0) (2020-07-18)

**Note:** Version bump only for package @deip/research-service





# [1.34.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.33.0...v1.34.0) (2020-07-15)

**Note:** Version bump only for package @deip/research-service





# [1.33.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.32.0...v1.33.0) (2020-07-10)

**Note:** Version bump only for package @deip/research-service





# [1.32.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.31.0...v1.32.0) (2020-07-10)

**Note:** Version bump only for package @deip/research-service





# [1.31.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.30.0...v1.31.0) (2020-07-09)


### Features

* **@deip/expertise-contributions-service:** Accounts ECI filter ([f11f3c4](https://gitlab.com/DEIP/deip-client-modules/commit/f11f3c4eb1eefc0b076c106f5228b3a9c88b473c))





# [1.28.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.27.0...v1.28.0) (2020-07-01)

**Note:** Version bump only for package @deip/research-service





# [1.27.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.26.0...v1.27.0) (2020-06-30)

**Note:** Version bump only for package @deip/research-service





# [1.26.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.25.0...v1.26.0) (2020-06-28)

**Note:** Version bump only for package @deip/research-service





# [1.25.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.24.0...v1.25.0) (2020-06-24)


### Features

* **@deip/rpc-client:** fetch all accounts method is deprecated ([7f5b8ee](https://gitlab.com/DEIP/deip-client-modules/commit/7f5b8ee8615193d5257a245e9e7dd5f99dd97a7b))





# [1.24.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.23.0...v1.24.0) (2020-06-22)


### Features

* **@deip/research-service:** Review share percent is optional ([25ac91b](https://gitlab.com/DEIP/deip-client-modules/commit/25ac91b69379342b2164826fc2aff9b1b9fb7f56))





# [1.23.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.22.0...v1.23.0) (2020-06-10)

**Note:** Version bump only for package @deip/research-service





# [1.22.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.21.0...v1.22.0) (2020-06-09)

**Note:** Version bump only for package @deip/research-service





# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)


### Features

* **@deip/research-group-service:** Posting authorities replaced with active overrides ([c832136](https://gitlab.com/DEIP/deip-client-modules/commit/c832136a7b0875171db05f69d445d1dd95d11379))





# [1.20.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.19.0...v1.20.0) (2020-05-27)


### Features

* **@deip/research-service:** Research application delete ([722e0cc](https://gitlab.com/DEIP/deip-client-modules/commit/722e0ccb48e7ddf1a57c8ee8ee23a4d660760df2))





# [1.19.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.18.0...v1.19.0) (2020-05-26)


### Features

* **@deip/research-service:** Research criterias ([f52c25e](https://gitlab.com/DEIP/deip-client-modules/commit/f52c25e4552d0f31f5ba42227d46edece4e7a60a))





# [1.18.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.17.0...v1.18.0) (2020-05-24)


### Features

* **@deip/research-service:** Research listing endpoints ([bc07d52](https://gitlab.com/DEIP/deip-client-modules/commit/bc07d52960a3b88aab590dc4d5b74b956d800fb4))





# [1.17.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.16.0...v1.17.0) (2020-05-22)


### Features

* **@deip/research-service:** Permlinks deprecated ([70f9b37](https://gitlab.com/DEIP/deip-client-modules/commit/70f9b37013a284e9ec1d2806952da6713b354cd4))





# [1.16.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.15.0...v1.16.0) (2020-05-21)


### Features

* **@deip/research-service:** Research application edit endpoint ([4467dea](https://gitlab.com/DEIP/deip-client-modules/commit/4467dea45dee80759eed4cc3baf35540cdbc3cc4))





# [1.15.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.14.0...v1.15.0) (2020-05-21)


### Features

* **@deip/research-service:** Research application approve/reject endpoints ([c9ed6ea](https://gitlab.com/DEIP/deip-client-modules/commit/c9ed6ea3d120f45015760cb4b398560e5e0a5a8d))





# [1.14.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.13.0...v1.14.0) (2020-05-20)


### Features

* **@deip/research-service:** Research application endpoints ([ba2985f](https://gitlab.com/DEIP/deip-client-modules/commit/ba2985fae23fd5c7a48d55acb8d7bf5a0e9181d1))





# [1.13.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.12.0...v1.13.0) (2020-05-18)

**Note:** Version bump only for package @deip/research-service





# [1.11.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.10.0...v1.11.0) (2020-05-14)


### Features

* **@deip/research-service:** Tenant dynamic criterias; ([f5fbd6e](https://gitlab.com/DEIP/deip-client-modules/commit/f5fbd6e7a4e59df5700d6125595d7ac06fe0413a))





# [1.10.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.9.0...v1.10.0) (2020-05-14)


### Features

* **@deip/proposals-service:** External id serializer; Services api params ([2c86e03](https://gitlab.com/DEIP/deip-client-modules/commit/2c86e03882b048bc0f20a754008188007bf5d4ef))





# [1.9.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.8.0...v1.9.0) (2020-05-07)

**Note:** Version bump only for package @deip/research-service





# [1.7.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.6.0...v1.7.0) (2020-05-04)


### Features

* **@deip/proposals-service:** Multisignature proposals ([a63a9b9](https://gitlab.com/DEIP/deip-client-modules/commit/a63a9b90f275718ef0b2ef13390c1d9d1391d84c))





# [1.6.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.5.0...v1.6.0) (2020-04-13)


### Features

* **@deip/grants-service:** Grant award withdrawal request api ([ea3e17c](https://gitlab.com/DEIP/deip-client-modules/commit/ea3e17c007b7df29ffdc001d73cd725c705c25ee))





# [1.5.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.4.0...v1.5.0) (2020-04-03)

**Note:** Version bump only for package @deip/research-service





# [1.4.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.3.0...v1.4.0) (2020-03-31)

**Note:** Version bump only for package @deip/research-service





# [1.3.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.2.0...v1.3.0) (2020-03-30)


### Features

* **@deip/expertise-contributions-service:** Expertise contributions service ([4d87bd0](https://gitlab.com/DEIP/deip-client-modules/commit/4d87bd039319569be494bd9249548451d3596aff))





# [1.2.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.1.0...v1.2.0) (2020-03-23)

**Note:** Version bump only for package @deip/research-service





# 1.1.0 (2020-03-20)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-19)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))
