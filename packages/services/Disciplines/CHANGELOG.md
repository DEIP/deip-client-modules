# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)

**Note:** Version bump only for package @deip/disciplines-service





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)

**Note:** Version bump only for package @deip/disciplines-service





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)

**Note:** Version bump only for package @deip/disciplines-service





# [1.88.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.87.0...v1.88.0) (2021-02-18)

**Note:** Version bump only for package @deip/disciplines-service





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)

**Note:** Version bump only for package @deip/disciplines-service





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/disciplines-service





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)

**Note:** Version bump only for package @deip/disciplines-service





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)

**Note:** Version bump only for package @deip/disciplines-service





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)


### Features

* **@deip/users-service:** moving deipRpc to the server ([c6b1d6e](https://gitlab.com/DEIP/deip-client-modules/commit/c6b1d6ec48f6a94b27abb0724e49ba2d63a57e0c))





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/disciplines-service





# [1.77.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.76.0...v1.77.0) (2021-01-14)


### Features

* **@deip/disciplines-service:** claims removed, plugin api added ([b35b45a](https://gitlab.com/DEIP/deip-client-modules/commit/b35b45a279ff023f2b9a3730340a3aa05164fa45))





# [1.76.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.1...v1.76.0) (2021-01-12)


### Features

* **@deip/disciplines-service:** fetching disciplines ([59964c4](https://gitlab.com/DEIP/deip-client-modules/commit/59964c428ed78af4a0d23ecfdd014155b3f37b29))





## [1.75.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.0...v1.75.1) (2021-01-05)

**Note:** Version bump only for package @deip/disciplines-service





# [1.66.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.65.0...v1.66.0) (2020-12-08)

**Note:** Version bump only for package @deip/disciplines-service





# [1.65.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.64.0...v1.65.0) (2020-12-07)

**Note:** Version bump only for package @deip/disciplines-service





# [1.62.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.61.0...v1.62.0) (2020-12-03)

**Note:** Version bump only for package @deip/disciplines-service





## [1.57.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.0...v1.57.1) (2020-11-19)

**Note:** Version bump only for package @deip/disciplines-service





# [1.57.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.56.0...v1.57.0) (2020-11-18)

**Note:** Version bump only for package @deip/disciplines-service





# [1.55.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.54.0...v1.55.0) (2020-11-06)

**Note:** Version bump only for package @deip/disciplines-service





# [1.54.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.53.0...v1.54.0) (2020-11-05)

**Note:** Version bump only for package @deip/disciplines-service





# [1.53.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.52.0...v1.53.0) (2020-11-04)

**Note:** Version bump only for package @deip/disciplines-service





# [1.52.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.51.0...v1.52.0) (2020-11-02)

**Note:** Version bump only for package @deip/disciplines-service





# [1.51.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.50.0...v1.51.0) (2020-10-27)

**Note:** Version bump only for package @deip/disciplines-service





# [1.48.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.47.0...v1.48.0) (2020-10-08)

**Note:** Version bump only for package @deip/disciplines-service





# [1.44.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.43.0...v1.44.0) (2020-10-02)

**Note:** Version bump only for package @deip/disciplines-service





# [1.37.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.36.0...v1.37.0) (2020-08-25)

**Note:** Version bump only for package @deip/disciplines-service





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)

**Note:** Version bump only for package @deip/disciplines-service





# [1.35.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.34.0...v1.35.0) (2020-07-18)

**Note:** Version bump only for package @deip/disciplines-service





# [1.34.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.33.0...v1.34.0) (2020-07-15)

**Note:** Version bump only for package @deip/disciplines-service





# [1.33.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.32.0...v1.33.0) (2020-07-10)

**Note:** Version bump only for package @deip/disciplines-service





# [1.32.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.31.0...v1.32.0) (2020-07-10)

**Note:** Version bump only for package @deip/disciplines-service





# [1.31.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.30.0...v1.31.0) (2020-07-09)

**Note:** Version bump only for package @deip/disciplines-service





# [1.28.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.27.0...v1.28.0) (2020-07-01)

**Note:** Version bump only for package @deip/disciplines-service





# [1.27.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.26.0...v1.27.0) (2020-06-30)

**Note:** Version bump only for package @deip/disciplines-service





# [1.26.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.25.0...v1.26.0) (2020-06-28)

**Note:** Version bump only for package @deip/disciplines-service





# [1.25.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.24.0...v1.25.0) (2020-06-24)

**Note:** Version bump only for package @deip/disciplines-service





# [1.24.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.23.0...v1.24.0) (2020-06-22)

**Note:** Version bump only for package @deip/disciplines-service





# [1.23.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.22.0...v1.23.0) (2020-06-10)

**Note:** Version bump only for package @deip/disciplines-service





# [1.22.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.21.0...v1.22.0) (2020-06-09)

**Note:** Version bump only for package @deip/disciplines-service





# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)

**Note:** Version bump only for package @deip/disciplines-service





# [1.18.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.17.0...v1.18.0) (2020-05-24)

**Note:** Version bump only for package @deip/disciplines-service





# [1.17.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.16.0...v1.17.0) (2020-05-22)

**Note:** Version bump only for package @deip/disciplines-service





# [1.13.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.12.0...v1.13.0) (2020-05-18)

**Note:** Version bump only for package @deip/disciplines-service





# [1.10.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.9.0...v1.10.0) (2020-05-14)

**Note:** Version bump only for package @deip/disciplines-service





# [1.7.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.6.0...v1.7.0) (2020-05-04)


### Features

* **@deip/proposals-service:** Multisignature proposals ([a63a9b9](https://gitlab.com/DEIP/deip-client-modules/commit/a63a9b90f275718ef0b2ef13390c1d9d1391d84c))





# [1.6.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.5.0...v1.6.0) (2020-04-13)

**Note:** Version bump only for package @deip/disciplines-service





# [1.5.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.4.0...v1.5.0) (2020-04-03)

**Note:** Version bump only for package @deip/disciplines-service





# [1.4.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.3.0...v1.4.0) (2020-03-31)

**Note:** Version bump only for package @deip/disciplines-service





# [1.3.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.2.0...v1.3.0) (2020-03-30)

**Note:** Version bump only for package @deip/disciplines-service





# [1.2.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.1.0...v1.2.0) (2020-03-23)

**Note:** Version bump only for package @deip/disciplines-service





# 1.1.0 (2020-03-20)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-19)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





## [1.0.2](https://gitlab.com/DEIP/deip-client-modules/compare/@deip/disciplines-service@1.0.1...@deip/disciplines-service@1.0.2) (2020-03-15)

**Note:** Version bump only for package @deip/disciplines-service
