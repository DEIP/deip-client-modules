# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)


### Features

* **@deip/research-nda-service:** Research nda list of projects ([24e9a85](https://gitlab.com/DEIP/deip-client-modules/commit/24e9a855d238afa6b6fe19d2a0bc308963a5b1f6))





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)


### Features

* **@deip/research-nda-service:** Transaction tenant signature ([29ede55](https://gitlab.com/DEIP/deip-client-modules/commit/29ede55b92bf126f176acb42515019b3af802d05))





# [1.88.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.87.0...v1.88.0) (2021-02-18)


### Features

* **@deip/research-nda-service:** Tenant signature ([413b775](https://gitlab.com/DEIP/deip-client-modules/commit/413b77559aedc3fb4427a2da7770703fe38e7e25))





# [1.87.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.86.0...v1.87.0) (2021-02-17)


### Features

* **@deip/research-nda-service:** NDA signatures ([9a76a45](https://gitlab.com/DEIP/deip-client-modules/commit/9a76a45ed4160707e44c51049e81d67430532799))





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)


### Features

* **@deip/research-nda-service:** NDA contracts ([6ec5d01](https://gitlab.com/DEIP/deip-client-modules/commit/6ec5d01244b9179b37c7e66fb61abbe3bff06518))
