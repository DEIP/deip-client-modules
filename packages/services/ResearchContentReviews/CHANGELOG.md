# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.94.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.93.0...v1.94.0) (2021-03-12)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.93.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.92.0...v1.93.0) (2021-03-09)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.90.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.89.0...v1.90.0) (2021-02-24)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)

**Note:** Version bump only for package @deip/research-content-reviews-service





## [1.88.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.0...v1.88.1) (2021-02-19)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.88.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.87.0...v1.88.0) (2021-02-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.87.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.86.0...v1.87.0) (2021-02-17)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)

**Note:** Version bump only for package @deip/research-content-reviews-service





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.84.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.83.0...v1.84.0) (2021-02-12)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)


### Features

* **@deip/users-service:** moving deipRpc to the server ([c6b1d6e](https://gitlab.com/DEIP/deip-client-modules/commit/c6b1d6ec48f6a94b27abb0724e49ba2d63a57e0c))





# [1.79.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.78.0...v1.79.0) (2021-01-22)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.77.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.76.0...v1.77.0) (2021-01-14)

**Note:** Version bump only for package @deip/research-content-reviews-service





## [1.75.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.0...v1.75.1) (2021-01-05)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.75.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.74.1...v1.75.0) (2020-12-29)

**Note:** Version bump only for package @deip/research-content-reviews-service





## [1.74.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.74.0...v1.74.1) (2020-12-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.74.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.73.0...v1.74.0) (2020-12-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.73.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.72.1...v1.73.0) (2020-12-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





## [1.72.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.72.0...v1.72.1) (2020-12-17)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.72.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.71.0...v1.72.0) (2020-12-17)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.71.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.70.0...v1.71.0) (2020-12-15)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.70.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.69.0...v1.70.0) (2020-12-15)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.69.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.68.0...v1.69.0) (2020-12-10)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.68.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.67.0...v1.68.0) (2020-12-09)


### Features

* **@deip/research-content-reviews-service:** Review criterias ([97b0639](https://gitlab.com/DEIP/deip-client-modules/commit/97b0639ca1007ba5dc447400014cde9bfdf6a45c))





# [1.67.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.66.0...v1.67.0) (2020-12-08)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.66.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.65.0...v1.66.0) (2020-12-08)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.65.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.64.0...v1.65.0) (2020-12-07)


### Features

* **@deip/research-service:** Research, ResearchContent, Review, Research Group meta ([6b70900](https://gitlab.com/DEIP/deip-client-modules/commit/6b70900e8ea2cc7c191d53434a6fca21b5d3a1ae))





# [1.64.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.63.0...v1.64.0) (2020-12-03)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.63.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.62.0...v1.63.0) (2020-12-03)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.62.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.61.0...v1.62.0) (2020-12-03)


### Features

* **@deip/research-group-service:** Research group metadata ([9cb3dbb](https://gitlab.com/DEIP/deip-client-modules/commit/9cb3dbb8e3c1843e366f3a48cdad1eecba93ce57))





# [1.59.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.58.0...v1.59.0) (2020-11-23)


### Features

* **@deip/proposals-service:** Unified proposals ([99036d3](https://gitlab.com/DEIP/deip-client-modules/commit/99036d39d787f9c839e8aeba33ba6d1dc1545e6a))





# [1.58.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.1...v1.58.0) (2020-11-20)

**Note:** Version bump only for package @deip/research-content-reviews-service





## [1.57.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.0...v1.57.1) (2020-11-19)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.57.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.56.0...v1.57.0) (2020-11-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.56.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.55.0...v1.56.0) (2020-11-11)


### Features

* **@deip/research-content-reviews-service:** Temp name for demo ([55d9cb7](https://gitlab.com/DEIP/deip-client-modules/commit/55d9cb725d8ee653ff0f19ca49b191bbfada9da2))





# [1.55.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.54.0...v1.55.0) (2020-11-06)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.54.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.53.0...v1.54.0) (2020-11-05)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.53.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.52.0...v1.53.0) (2020-11-04)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.52.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.51.0...v1.52.0) (2020-11-02)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.51.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.50.0...v1.51.0) (2020-10-27)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.48.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.47.0...v1.48.0) (2020-10-08)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.47.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.46.0...v1.47.0) (2020-10-06)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.46.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.45.0...v1.46.0) (2020-10-04)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.45.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.44.0...v1.45.0) (2020-10-02)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.44.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.43.0...v1.44.0) (2020-10-02)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.43.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.42.0...v1.43.0) (2020-09-28)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.42.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.41.0...v1.42.0) (2020-09-27)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.41.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.40.0...v1.41.0) (2020-09-21)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.40.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.39.0...v1.40.0) (2020-09-21)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.39.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.38.0...v1.39.0) (2020-09-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.38.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.37.0...v1.38.0) (2020-09-10)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.37.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.36.0...v1.37.0) (2020-08-25)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.35.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.34.0...v1.35.0) (2020-07-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.34.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.33.0...v1.34.0) (2020-07-15)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.33.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.32.0...v1.33.0) (2020-07-10)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.32.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.31.0...v1.32.0) (2020-07-10)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.31.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.30.0...v1.31.0) (2020-07-09)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.28.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.27.0...v1.28.0) (2020-07-01)


### Features

* **@deip/rpc-client:** Discipline ECI stats endpoint ([8f6d879](https://gitlab.com/DEIP/deip-client-modules/commit/8f6d87903fce8bf6cfbb53b53834c7d34ab5ab8e))





# [1.27.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.26.0...v1.27.0) (2020-06-30)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.26.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.25.0...v1.26.0) (2020-06-28)


### Features

* **@deip/rpc-client:** Accounts ECI stats endpoint ([142ed5c](https://gitlab.com/DEIP/deip-client-modules/commit/142ed5cf0643d1535fede966c9f64cdf55b1ed7b))





# [1.25.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.24.0...v1.25.0) (2020-06-24)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.24.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.23.0...v1.24.0) (2020-06-22)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.23.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.22.0...v1.23.0) (2020-06-10)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.22.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.21.0...v1.22.0) (2020-06-09)


### Features

* **@deip/research-service:** Discipline external ids ([04e64aa](https://gitlab.com/DEIP/deip-client-modules/commit/04e64aa25998ed992e12167318d747ae33ddfc96))





# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)


### Features

* **@deip/research-group-service:** Posting authorities replaced with active overrides ([c832136](https://gitlab.com/DEIP/deip-client-modules/commit/c832136a7b0875171db05f69d445d1dd95d11379))





# [1.20.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.19.0...v1.20.0) (2020-05-27)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.19.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.18.0...v1.19.0) (2020-05-26)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.18.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.17.0...v1.18.0) (2020-05-24)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.17.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.16.0...v1.17.0) (2020-05-22)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.16.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.15.0...v1.16.0) (2020-05-21)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.15.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.14.0...v1.15.0) (2020-05-21)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.14.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.13.0...v1.14.0) (2020-05-20)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.13.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.12.0...v1.13.0) (2020-05-18)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.11.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.10.0...v1.11.0) (2020-05-14)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.10.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.9.0...v1.10.0) (2020-05-14)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.9.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.8.0...v1.9.0) (2020-05-07)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.7.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.6.0...v1.7.0) (2020-05-04)


### Features

* **@deip/proposals-service:** Multisignature proposals ([a63a9b9](https://gitlab.com/DEIP/deip-client-modules/commit/a63a9b90f275718ef0b2ef13390c1d9d1391d84c))





# [1.6.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.5.0...v1.6.0) (2020-04-13)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.5.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.4.0...v1.5.0) (2020-04-03)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.4.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.3.0...v1.4.0) (2020-03-31)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.3.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.2.0...v1.3.0) (2020-03-30)

**Note:** Version bump only for package @deip/research-content-reviews-service





# [1.2.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.1.0...v1.2.0) (2020-03-23)

**Note:** Version bump only for package @deip/research-content-reviews-service





# 1.1.0 (2020-03-20)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))


### Features

* **@deip/research-content-reviews-service:** generic assessment model for review ([7e543e2](https://gitlab.com/DEIP/deip-client-modules/commit/7e543e224f93ada160f2c91a133db0cba5c0f5b2))





# 1.0.0 (2020-03-19)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





## [1.0.2](https://gitlab.com/DEIP/deip-client-modules/compare/@deip/research-content-reviews-service@1.0.1...@deip/research-content-reviews-service@1.0.2) (2020-03-15)

**Note:** Version bump only for package @deip/research-content-reviews-service
