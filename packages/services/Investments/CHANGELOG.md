# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)

**Note:** Version bump only for package @deip/investments-service





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)

**Note:** Version bump only for package @deip/investments-service





# [1.90.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.89.0...v1.90.0) (2021-02-24)


### Features

* **@deip/research-service:** Research group owner for TTO is set to ([9a15de7](https://gitlab.com/DEIP/deip-client-modules/commit/9a15de755e58c1a0152d67adf05d0b000cccbbce))





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)


### Features

* **@deip/research-nda-service:** Transaction tenant signature ([29ede55](https://gitlab.com/DEIP/deip-client-modules/commit/29ede55b92bf126f176acb42515019b3af802d05))





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)

**Note:** Version bump only for package @deip/investments-service





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/investments-service





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)

**Note:** Version bump only for package @deip/investments-service





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)


### Features

* **@deip/users-service:** deipRpc calls moved to the server ([3787fd6](https://gitlab.com/DEIP/deip-client-modules/commit/3787fd69117ddec1121000611c4282fad183af13))





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)


### Features

* **@deip/users-service:** moving deipRpc to the server ([c6b1d6e](https://gitlab.com/DEIP/deip-client-modules/commit/c6b1d6ec48f6a94b27abb0724e49ba2d63a57e0c))





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/investments-service





# [1.77.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.76.0...v1.77.0) (2021-01-14)

**Note:** Version bump only for package @deip/investments-service





## [1.75.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.0...v1.75.1) (2021-01-05)

**Note:** Version bump only for package @deip/investments-service





# [1.67.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.66.0...v1.67.0) (2020-12-08)


### Features

* **@deip/assets-service:** Security token asset ([cef6d02](https://gitlab.com/DEIP/deip-client-modules/commit/cef6d025ae9d2ba3f72574ccbb0a8853d26f66d0))





# [1.66.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.65.0...v1.66.0) (2020-12-08)


### Features

* **@deip/research-service:** Licensing revenue share ([007b8ec](https://gitlab.com/DEIP/deip-client-modules/commit/007b8ec7e9cfad6cf8cff4064c11d4f7f513b89d))





# [1.65.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.64.0...v1.65.0) (2020-12-07)

**Note:** Version bump only for package @deip/investments-service





# [1.62.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.61.0...v1.62.0) (2020-12-03)

**Note:** Version bump only for package @deip/investments-service





# [1.59.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.58.0...v1.59.0) (2020-11-23)


### Features

* **@deip/proposals-service:** Unified proposals ([99036d3](https://gitlab.com/DEIP/deip-client-modules/commit/99036d39d787f9c839e8aeba33ba6d1dc1545e6a))





## [1.57.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.0...v1.57.1) (2020-11-19)

**Note:** Version bump only for package @deip/investments-service





# [1.57.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.56.0...v1.57.0) (2020-11-18)

**Note:** Version bump only for package @deip/investments-service





# [1.55.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.54.0...v1.55.0) (2020-11-06)


### Features

* **@deip/assets-service:** Assets revenue api ([f682066](https://gitlab.com/DEIP/deip-client-modules/commit/f682066528930170e9654c2a99a1c3bbeef44c74))





# [1.54.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.53.0...v1.54.0) (2020-11-05)


### Features

* **@deip/assets-service:** Security tokens incorporated to assets ([6ff01ee](https://gitlab.com/DEIP/deip-client-modules/commit/6ff01ee0489a95d321208c7e1196600e38517060))





# [1.53.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.52.0...v1.53.0) (2020-11-04)


### Features

* **@deip/investments-service:** Transfer security token memo ([2305fca](https://gitlab.com/DEIP/deip-client-modules/commit/2305fcac34e8afac55dce606f1e24735eee68f7f))





# [1.52.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.51.0...v1.52.0) (2020-11-02)


### Features

* **@deip/investments-service:** Fundraising fix ([6a69520](https://gitlab.com/DEIP/deip-client-modules/commit/6a695204464aaeda21112c0bf3bd58773042293d))





# [1.51.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.50.0...v1.51.0) (2020-10-27)


### Features

* **@deip/investments-service:** Security tokens ([5931015](https://gitlab.com/DEIP/deip-client-modules/commit/5931015f61d871b022811cc20e032b34a4b451db))





# [1.48.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.47.0...v1.48.0) (2020-10-08)

**Note:** Version bump only for package @deip/investments-service





# [1.44.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.43.0...v1.44.0) (2020-10-02)

**Note:** Version bump only for package @deip/investments-service





# [1.37.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.36.0...v1.37.0) (2020-08-25)

**Note:** Version bump only for package @deip/investments-service





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)

**Note:** Version bump only for package @deip/investments-service





# [1.35.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.34.0...v1.35.0) (2020-07-18)

**Note:** Version bump only for package @deip/investments-service





# [1.34.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.33.0...v1.34.0) (2020-07-15)

**Note:** Version bump only for package @deip/investments-service





# [1.33.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.32.0...v1.33.0) (2020-07-10)

**Note:** Version bump only for package @deip/investments-service





# [1.32.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.31.0...v1.32.0) (2020-07-10)

**Note:** Version bump only for package @deip/investments-service





# [1.31.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.30.0...v1.31.0) (2020-07-09)

**Note:** Version bump only for package @deip/investments-service





# [1.28.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.27.0...v1.28.0) (2020-07-01)

**Note:** Version bump only for package @deip/investments-service





# [1.27.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.26.0...v1.27.0) (2020-06-30)

**Note:** Version bump only for package @deip/investments-service





# [1.26.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.25.0...v1.26.0) (2020-06-28)

**Note:** Version bump only for package @deip/investments-service





# [1.25.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.24.0...v1.25.0) (2020-06-24)

**Note:** Version bump only for package @deip/investments-service





# [1.24.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.23.0...v1.24.0) (2020-06-22)

**Note:** Version bump only for package @deip/investments-service





# [1.23.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.22.0...v1.23.0) (2020-06-10)

**Note:** Version bump only for package @deip/investments-service





# [1.22.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.21.0...v1.22.0) (2020-06-09)

**Note:** Version bump only for package @deip/investments-service





# [1.21.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.20.0...v1.21.0) (2020-06-05)

**Note:** Version bump only for package @deip/investments-service





# [1.18.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.17.0...v1.18.0) (2020-05-24)

**Note:** Version bump only for package @deip/investments-service





# [1.17.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.16.0...v1.17.0) (2020-05-22)

**Note:** Version bump only for package @deip/investments-service





# [1.13.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.12.0...v1.13.0) (2020-05-18)

**Note:** Version bump only for package @deip/investments-service





# [1.10.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.9.0...v1.10.0) (2020-05-14)

**Note:** Version bump only for package @deip/investments-service





# [1.7.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.6.0...v1.7.0) (2020-05-04)

**Note:** Version bump only for package @deip/investments-service





# [1.6.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.5.0...v1.6.0) (2020-04-13)

**Note:** Version bump only for package @deip/investments-service





# [1.5.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.4.0...v1.5.0) (2020-04-03)

**Note:** Version bump only for package @deip/investments-service





# [1.4.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.3.0...v1.4.0) (2020-03-31)

**Note:** Version bump only for package @deip/investments-service





# [1.3.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.2.0...v1.3.0) (2020-03-30)

**Note:** Version bump only for package @deip/investments-service





# [1.2.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.1.0...v1.2.0) (2020-03-23)

**Note:** Version bump only for package @deip/investments-service





# 1.1.0 (2020-03-20)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-19)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))





# 1.0.0 (2020-03-18)


### Bug Fixes

* deipRpc config ([879ce0c](https://gitlab.com/DEIP/deip-client-modules/commit/879ce0c369bdfa741f7044f4b14fa2990c767f99))
