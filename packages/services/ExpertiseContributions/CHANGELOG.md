# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/expertise-contributions-service





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)


### Features

* **@deip/users-service:** deipRpc calls moved to the server ([3787fd6](https://gitlab.com/DEIP/deip-client-modules/commit/3787fd69117ddec1121000611c4282fad183af13))





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)


### Features

* **@deip/users-service:** moving deipRpc to the server ([c6b1d6e](https://gitlab.com/DEIP/deip-client-modules/commit/c6b1d6ec48f6a94b27abb0724e49ba2d63a57e0c))





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/expertise-contributions-service





# [1.36.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.35.0...v1.36.0) (2020-08-24)

**Note:** Version bump only for package @deip/expertise-contributions-service





# [1.35.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.34.0...v1.35.0) (2020-07-18)


### Features

* **@deip/expertise-contributions-service:** Research\ResearchContent ECI stats ([1ccfc59](https://gitlab.com/DEIP/deip-client-modules/commit/1ccfc59dab31021959a305b6dd3d2eb5a17804d9))





# [1.34.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.33.0...v1.34.0) (2020-07-15)


### Features

* **@deip/expertise-contributions-service:** Growth rate starting point ([0d013b5](https://gitlab.com/DEIP/deip-client-modules/commit/0d013b53a0d3a28a813c8e725577b07824f534c5))





# [1.33.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.32.0...v1.33.0) (2020-07-10)


### Features

* **@deip/expertise-contributions-service:** Disciplines ECI history endpoint ([acd85ef](https://gitlab.com/DEIP/deip-client-modules/commit/acd85ef9c6fd5134f2f7cf013225cfbf59ad2acb))





# [1.32.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.31.0...v1.32.0) (2020-07-10)


### Features

* **@deip/expertise-contributions-service:** Disciplines ECI growth rate ([a9eb1a0](https://gitlab.com/DEIP/deip-client-modules/commit/a9eb1a005106aab37db17007752d1880a130d65d))





# [1.31.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.30.0...v1.31.0) (2020-07-09)


### Features

* **@deip/expertise-contributions-service:** Accounts ECI filter ([f11f3c4](https://gitlab.com/DEIP/deip-client-modules/commit/f11f3c4eb1eefc0b076c106f5228b3a9c88b473c))





# [1.30.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.29.0...v1.30.0) (2020-07-02)


### Features

* **@deip/expertise-contributions-service:** Research contents eci stat ([ff0c26e](https://gitlab.com/DEIP/deip-client-modules/commit/ff0c26e040ef05e639533c490bdf29532fe0d055))





# [1.29.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.28.0...v1.29.0) (2020-07-02)


### Features

* **@deip/expertise-contributions-service:** Account eci stat ([652fea8](https://gitlab.com/DEIP/deip-client-modules/commit/652fea8945c26a866d694cbd8559b95ae058f471))





# [1.28.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.27.0...v1.28.0) (2020-07-01)


### Features

* **@deip/rpc-client:** Discipline ECI stats endpoint ([8f6d879](https://gitlab.com/DEIP/deip-client-modules/commit/8f6d87903fce8bf6cfbb53b53834c7d34ab5ab8e))





# [1.26.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.25.0...v1.26.0) (2020-06-28)


### Features

* **@deip/rpc-client:** Accounts ECI stats endpoint ([142ed5c](https://gitlab.com/DEIP/deip-client-modules/commit/142ed5cf0643d1535fede966c9f64cdf55b1ed7b))





# [1.3.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.2.0...v1.3.0) (2020-03-30)


### Features

* **@deip/expertise-contributions-service:** Expertise contributions service ([4d87bd0](https://gitlab.com/DEIP/deip-client-modules/commit/4d87bd039319569be494bd9249548451d3596aff))
