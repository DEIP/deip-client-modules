# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.92.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.91.0...v1.92.0) (2021-03-03)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.91.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.90.0...v1.91.0) (2021-03-03)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.89.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.88.1...v1.89.0) (2021-02-23)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.88.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.87.0...v1.88.0) (2021-02-18)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.87.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.86.0...v1.87.0) (2021-02-17)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.86.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.1...v1.86.0) (2021-02-17)


### Features

* **@deip/research-nda-service:** NDA contracts ([6ec5d01](https://gitlab.com/DEIP/deip-client-modules/commit/6ec5d01244b9179b37c7e66fb61abbe3bff06518))





## [1.85.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.85.0...v1.85.1) (2021-02-17)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.82.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.81.0...v1.82.0) (2021-02-10)


### Features

* **@deip/express-licensing-service:** Express licensing fixed ([4a81f8d](https://gitlab.com/DEIP/deip-client-modules/commit/4a81f8d80fdb1729e047db719be27440c278c946))





# [1.81.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.80.0...v1.81.0) (2021-02-08)


### Features

* **@deip/users-service:** deipRpc calls moved to the server ([3787fd6](https://gitlab.com/DEIP/deip-client-modules/commit/3787fd69117ddec1121000611c4282fad183af13))





# [1.80.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.79.1...v1.80.0) (2021-01-27)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.78.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.77.0...v1.78.0) (2021-01-22)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.77.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.76.0...v1.77.0) (2021-01-14)

**Note:** Version bump only for package @deip/express-licensing-service





## [1.75.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.75.0...v1.75.1) (2021-01-05)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.66.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.65.0...v1.66.0) (2020-12-08)


### Features

* **@deip/research-service:** Licensing revenue share ([007b8ec](https://gitlab.com/DEIP/deip-client-modules/commit/007b8ec7e9cfad6cf8cff4064c11d4f7f513b89d))





# [1.65.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.64.0...v1.65.0) (2020-12-07)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.62.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.61.0...v1.62.0) (2020-12-03)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.60.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.59.0...v1.60.0) (2020-11-24)


### Features

* **@deip/proposals-service:** Proposals update endpoint ([21ba24b](https://gitlab.com/DEIP/deip-client-modules/commit/21ba24bcc8c67f6e1ecc7b170239d52a55dabca1))





# [1.59.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.58.0...v1.59.0) (2020-11-23)


### Features

* **@deip/proposals-service:** Unified proposals ([99036d3](https://gitlab.com/DEIP/deip-client-modules/commit/99036d39d787f9c839e8aeba33ba6d1dc1545e6a))





# [1.58.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.1...v1.58.0) (2020-11-20)

**Note:** Version bump only for package @deip/express-licensing-service





## [1.57.1](https://gitlab.com/DEIP/deip-client-modules/compare/v1.57.0...v1.57.1) (2020-11-19)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.57.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.56.0...v1.57.0) (2020-11-18)


### Features

* **@deip/proposals-service:** Proposals for account ([a03e847](https://gitlab.com/DEIP/deip-client-modules/commit/a03e847bb8a1b8b93dec3d4a77f43fb8a5db9410))





# [1.55.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.54.0...v1.55.0) (2020-11-06)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.54.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.53.0...v1.54.0) (2020-11-05)


### Features

* **@deip/assets-service:** Security tokens incorporated to assets ([6ff01ee](https://gitlab.com/DEIP/deip-client-modules/commit/6ff01ee0489a95d321208c7e1196600e38517060))





# [1.53.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.52.0...v1.53.0) (2020-11-04)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.52.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.51.0...v1.52.0) (2020-11-02)

**Note:** Version bump only for package @deip/express-licensing-service





# [1.51.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.50.0...v1.51.0) (2020-10-27)


### Features

* **@deip/investments-service:** Security tokens ([5931015](https://gitlab.com/DEIP/deip-client-modules/commit/5931015f61d871b022811cc20e032b34a4b451db))





# [1.49.0](https://gitlab.com/DEIP/deip-client-modules/compare/v1.48.0...v1.49.0) (2020-10-09)


### Features

* **@deip/express-licensing-service:** Express licensing feature ([bbbadfc](https://gitlab.com/DEIP/deip-client-modules/commit/bbbadfcdc5773a2214391f761057d5ce013b902a))
